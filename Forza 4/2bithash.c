#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include "uthash.h"



/* COSTANTI */
#define P1 0 /* Giocatore umano */
#define P2 1 /* Giocatore CPU */

#define BLANK ' ' /* Token caselle vuote, codificato con 00 */
#define TOKP1 'O' /* Token giocatore 1, codificato con 01 */
#define TOKP2 'X' /* Token giocatore 2, codificato con 10 */

#define TIEVAL -1 /* Punteggio di -1 associato con gli stati pareggio */

#define AGE_CUTOFF (maxplies/2) /* Età massima consentita alle entry della tabella hash prima che siano dichiarate da scartare e aggiornare */



/* DEFINIZIONI DI TIPO E DEFAULT */
/* Struct necessaria per l'hashing. */
struct hashScore {
	uint8_t *key; /* Chiave che identifica lo stato da salvare in tabella hash; DEVE ESSERE UNIVOCA! */
	int score; /* Punteggio associato con lo stato corrispondente. */
	int turn; /* Vale -1 se il valore è esatto; altrimenti (cioè se sono state coinvolte delle eval nel calcolo) è il turno di gioco in cui il punteggio è stato calcolato. */
	UT_hash_handle hh; /* Richiesta da UThash. */
};

/* Struct per i valori di ritorno delle chiamate minimax. */
struct mmretval {
	int score; /* Punteggio */
	char isExact; /* Vale 1 se il punteggio è esatto, 0 se sono coinvolte delle eval. */
};

/* Costanti per l'inizializzazione dei mmretval. */
const struct mmretval DEF_MAX_MMRETVAL = {
	INT_MIN, /* score */
	1 /* isExact */
};

const struct mmretval DEF_MIN_MMRETVAL = {
	INT_MAX, /* score */
	1 /* isExact */
};



/* STUB DI FUNZIONE */
void setTok(uint8_t *b, char tok, int row, int col);
char getTok(int row, int col);
void printBoard();
uint8_t* copyBoard();
int eval();
int getNextBlank(int col);
int isWinMove(int row, int col);
int getNextP2Move();
struct mmretval maxMove(int a, int b, int ply);
struct mmretval minMove(int a, int b, int ply);



/* VARIABILI GLOBALI */
/* Tabella has di trasposizione per gli stati già visitati, con i relativi punteggi. */
struct hashScore *scoreTable = NULL;

/* Tabellone di gioco di dimensioni (logiche) rows * cols. Gli elementi possono essere (la rappresentazione binaria di) BLANK, TOKP1, TOKP2.
Viene inizializzata a tutti BLANK (0). */
uint8_t *board;

/* Dimensioni (logiche) del tabellone di gioco. Passate opzionalmente da riga di comando, i valori predefiniti sono quelli standard per Forza Quattro. */
int rows = 6;
int cols = 7;

/* Lunghezza (fisica) del vettore che rappresenta il tabellone di gioco. */
int len;

/* Numero di ply espansi dal minimax prima di ricorrere ad una valutazione euristica. Opzionalmente passato da riga di comando. */
int maxplies = 7;

/* Indicatore del turno di gioco corrente. */
int gameturn = 0;



/* FUNZIONI */
void setTok(uint8_t *b, char tok, int row, int col) {
/* REQUIRES: tok = TOKP1 o TOKP2 o BLANK; 0 <= row < rows; 0 <= col < cols; board ben inizializzata.
EFFECT: imposta la casella di posizione logica [row][col] col token tok. */
/*	Memo: BLANK è 00, TOKP1 è 01, TOKP2 è 10. */
	int pos = (row*cols +col)/4;

	switch ((row*cols +col) % 4) {
	case 0: /* Bit 1 e 2 */
		b[pos] &= 0x3F; /* 0011,1111 */
		switch (tok) {
		case TOKP1:
			b[pos] |= 0x40; /* 0100,0000 */
			break;
		case TOKP2:
			b[pos] |= 0x80; /* 1000,0000 */
			break;
		default:
			/* Non serve far nulla. */
			break;
		}
		break;
	case 1: /* Bit 3 e 4 */
		b[pos] &= 0xCF; /* 1100,1111 */
		switch (tok) {
		case TOKP1:
			b[pos] |= 0x10; /* 0001,0000 */
			break;
		case TOKP2:
			b[pos] |= 0x20; /* 0010,0000 */
			break;
		default:
			/* Non serve far nulla. */
			break;
		}
		break;
	case 2: /* Bit 5 e 6 */
		b[pos] &= 0xF3; /* 1111,0011 */
		switch (tok) {
		case TOKP1:
			b[pos] |= 0x04; /* 0000,0100 */
			break;
		case TOKP2:
			b[pos] |= 0x08; /* 0000,1000 */
			break;
		default:
			/* Non serve far nulla. */
			break;
		}
		break;
	case 3: /* Bit 7 e 8 */
		b[pos] &= 0xFC; /* 1111,1100 */
		switch (tok) {
		case TOKP1:
			b[pos] |= 0x01; /* 0000,0001 */
			break;
		case TOKP2:
			b[pos] |= 0x02; /* 0000,0010 */
			break;
		default:
			/* Non serve far nulla. */
			break;
		}
	}
}


char getTok(int row, int col) {
/* REQUIRES: 0 <= row < rows; 0 <= col < cols; board ben inizializzata.
EFFECT: ritorna il char (TOKP1, TOKP2 o BLANK) che si trova sul tabellone in posizione logica [row][col]. */
/*	Memo: BLANK è 00, TOKP1 è 01, TOKP2 è 10. */
	int pos = (row*cols +col)/4;
	
	switch ((row*cols +col) % 4) {
	case 0: /* Bit 1 e 2 */
		switch (board[pos] & 0xC0) { /* 1100,0000 */
		case 0x40: /* 0100,0000 */
			return TOKP1;
		case 0x80: /* 1000,0000 */
			return TOKP2;
		default:
			return BLANK;
		}
		break;
	case 1: /* Bit 3 e 4 */
		switch (board[pos] & 0x30) { /* 0011,0000 */
		case 0x10: /* 0001,0000 */
			return TOKP1;
		case 0x20: /* 0010,0000 */
			return TOKP2;
		default:
			return BLANK; 
		}
		break;
	case 2: /* Bit 5 e 6 */
		switch (board[pos] & 0x0C) { /* 0000,1100 */
		case 0x04: /* 0000,0100 */
			return TOKP1;
		case 0x08: /* 0000,1000 */
			return TOKP2;
		default:
			return BLANK;
		}
		break;
	case 3: /* Bit 7 e 8 */
		switch (board[pos] & 0x03) { /* 0000,0011 */
		case 0x01: /* 0000,0001 */
			return TOKP1;
		case 0x02: /* 0000,0010 */
			return TOKP2;
		default:
			return BLANK;
		}
		break;
	}
}


void printBoard() {
/* EFFECT: Stampa il tabellone su stdout in maniera leggibile. */
	int i,j;
	
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			printf("|%c", getTok(i,j));
		}
		printf("|\n");
	}
	for (j = 0; j < 2*cols +1; j++) {
		putchar('-');
	}
	putchar('\n');
}


uint8_t* copyBoard() {
/* REQUIRES: board ben inizializzata.
EFFECT: istanzia una copia di board.
RETURNS: puntatore ad una copia di board. */
	uint8_t *a;
	
	/* Si noti che sizeof(uint8_t) + 1. */
	if ((a = malloc(len)) == NULL) {
		perror("copyBoard malloc");
		exit(EXIT_FAILURE);
	}
	a = memcpy(a, board, len);
	return a;
}


int eval() {
/* REQUIRES: board ben inizializzata.
RETURNS: una valutazione euristica dello stato corrente della partita. Conta due punti per paio di TOKP2 in contatto su qualsiasi delle otto direzioni (logiche). Questo favorisce stati con raggruppamenti o file di token. */
	int i, j;
	int retval = 0;
	
	/* Per evitare di controllare se gli accessi siano legali per via dei bordi (logici), ripartiamo board in sezioni trattate separatamente. Tutti i controlli sono elencati in senso orario a partire dalle ore 12. */
	
	/* Angolo in alto a destra */
	if (getTok(0,cols-1) == TOKP2) {
		retval += (getTok(1,cols-1) == TOKP2) + (getTok(1,cols-2) == TOKP2) + (getTok(0,cols-2) == TOKP2);
	}
	
	/* Angolo in basso a destra */
	if (getTok(rows-1,cols-1) == TOKP2) {
		retval += (getTok(rows-2,cols-1) == TOKP2) + (getTok(rows-1,cols-2) == TOKP2) + (getTok(rows-2,cols-2) == TOKP2);
	}
	
	/* Angolo in basso a sinistra */
	if (getTok(rows-1,0) == TOKP2) {
		retval += (getTok(rows-2,0) == TOKP2) + (getTok(rows-2,1) == TOKP2) + (getTok(rows-1,1) == TOKP2);
	}
	
	/* Angolo in alto a sinistra */
	if (getTok(0,0) == TOKP2) {
		retval += (getTok(0,1) == TOKP2) + (getTok(1,1) == TOKP2) + (getTok(1,0) == TOKP2);
	}
	
	/* Riga in alto */
	for (j = 1; j < cols-1; j++) {
		if (getTok(0,j) == TOKP2) {
			retval += (getTok(0,j+1) == TOKP2) + (getTok(1,j+1) == TOKP2) + (getTok(1,j) == TOKP2) + (getTok(1,j-1) == TOKP2) + (getTok(0,j-1) == TOKP2);	
		}
	}
	
	/* Colonna destra */
	for (i = 1; i < rows-1; i++) {
		if (getTok(i,cols-1) == TOKP2) {
			retval += (getTok(i-1,cols-1) == TOKP2) + (getTok(i+1,cols-1) == TOKP2) + (getTok(i+1,cols-2) == TOKP2) + (getTok(i,cols-2) == TOKP2) + (getTok(i-1,cols-2) == TOKP2);
		}
	}
	
	/* Riga in basso */
	for (j = 1; j < cols-1; j++) {
		if (getTok(rows-1,j) == TOKP2) {
			retval += (getTok(rows-2,j) == TOKP2) + (getTok(rows-2,j+1) == TOKP2) + (getTok(rows-1,j+1) == TOKP2) + (getTok(rows-1,j-1) == TOKP2) + (getTok(rows-2,j-1) == TOKP2);
		}
	}
	
	/* Colonna sinistra */
	for (i = 1; i < rows-1; i++) {
		if (getTok(i,0) == TOKP2) {
			retval += (getTok(i-1,0) == TOKP2) + (getTok(i-1,1) == TOKP2) + (getTok(i,1) == TOKP2) + (getTok(i+1,1) == TOKP2) + (getTok(i+1,0) == TOKP2);
		}
	}
	
	/* Centro */
	for (i = 1; i < rows-1; i++) {
		for (j = 1; j < cols-1; j++) {
			if (getTok(i,j) == TOKP2) {
				retval += (getTok(i-1,j) == TOKP2) + (getTok(i-1,j+1) == TOKP2) + (getTok(i,j+1) == TOKP2) + (getTok(i+1,j+1) == TOKP2) + (getTok(i+1,j) == TOKP2) + (getTok(i+1,j-1) == TOKP2) + (getTok(i,j-1) == TOKP2) + (getTok(i-1,j-1) == TOKP2);
			}
		}
	}
	
	return retval;
}

int getNextBlank(int col) {
/* REQUIRES: 0 <= col < cols, board ben inizializzata.
EFFECT: Determina se sia possibile aggiungere un token alla colonna logica col, e dove.
RETURNS -1 se nessuna mossa e' possibile (colonna logica piena), altrimenti la riga logica più in basso (cioè maggiore) che abbia un BLANK nella colonna logica col. */
	int i = -1;
	
	while (i + 1 < rows && getTok(i+1,col) == BLANK) {
		i++;
	}
	
	return i;
}


int isWinMove(int row, int col) {
/* REQUIRES: 0 <= row < rows, 0 <= col < cols, board ben inizializzata, getTok(row,col) != BLANK.
EFFECT: determina se il token in posizione logica [row][col] sia parte di una quartina vincente per il giocatore corrispondente. La funzione è pensata per essere chiamata dopo aver effettuato una mossa.
RETURNS 1 per una vittoria, 0 altrimenti. */
	int i, j, streak;
	char token = getTok(row,col);
	
	/* L'elemento centrale è contato due volte, quindi streak parte da -1. Le direzioni nei commenti che seguono sono da intendersi logiche.
	Diagonale, verso l'alto. */
	streak = -1;
	i = row;
	j = col;
	while (0 <= i && i < rows && 0 <= j && j < cols) {
		if (getTok(i,j) == token) {
			streak++;
			i--;
			j--;
		} else {
			break;
		}
	}
	/* Diagonale, verso il basso. */
	i = row;
	j = col;
	while (0 <= i && i < rows && 0 <= j && j < cols) {
		if (getTok(i,j) == token) {
			streak++;
			i++;
			j++;
		} else {
			break;
		}
	}
	if (streak >= 4) {
		return 1;
	}
	
	/* Antidiagonale, verso l'alto. */
	streak = -1;
	i = row;
	j = col;
	while (0 <= i && i < rows && 0 <= j && j < cols) {
		if (getTok(i,j) == token) {
			streak++;
			i--;
			j++;
		} else {
			break;
		}
	}
	/* Antidiagonale, verso il basso. */
	i = row;
	j = col;
	while (0 <= i && i < rows && 0 <= j && j < cols) {
		if (getTok(i,j) == token) {
			streak++;
			i++;
			j--;
		} else {
			break;
		}
	}
	if (streak >= 4) {
		return 1;
	}
	
	/* Orizzontale, verso sinistra. */
	streak = -1;
	j = col;
	while (0 <= j && j < cols) {
		if (getTok(row,j) == token) {
			streak++;
			j--;
		} else {
			break;
		}
	}
	/* Orizzontale, verso destra. */
	j = col;
	while (0 <= j && j < cols) {
		if (getTok(row,j) == token) {
			streak++;
			j++;
		} else {
			break;
		}
	}
	if (streak >= 4) {
		return 1;
	}
	
	/* Verticale, verso l'alto non è necessaria e quindi streak parte da 0 stavolta.
	Verticale, verso il basso. */
	streak = 0;
	i = row;
	while (0 <= i && i < rows) {
		if (getTok(i,col) == token) {
			streak++;
			i++;
		} else {
			break;
		}
	}
	if (streak >= 4) {
		return 1;
	}
	
	/* Il token in posizione logica [row][col] non determina una vittoria. */
	return 0;
}


int getNextP2Move() {
/* REQUIRES: board ben inizializzata, il turno di gioco spetta a P2.
EFFECT: trova la mossa migliore per P2.
RETURNS: la mossa migliore o -1 se non ci sono mosse possibili (un pareggio). */
	int i, j, tmp;
	int maxScore = INT_MIN;
	int move = -1;
	
	/* Cerca successori. */
	for (j = 0; j < cols; j++) {
		if ((i = getNextBlank(j)) != -1) {
			/* E' una mossa valida. */
			setTok(board,TOKP2,i,j);
			if (isWinMove(i, j)) {
				 /* La mossa porta immediatamente a vittoria. */
				setTok(board,BLANK,i,j);
				return j;
			}
			/* Non c'è vittoria immediata, è necessaria una ricerca minimax. */
			if ((tmp = minMove(INT_MIN, INT_MAX, 2).score)>= maxScore) {
				maxScore = tmp;
				move = j;
			}
			setTok(board,BLANK,i,j);
			if (tmp == INT_MAX) {
				/* La mossa porta a vittoria. */
				return j;
			}
		}
	}
	
	return move;
}


struct mmretval maxMove(int a, int b, int ply) {
/* REQUIRES: a, b parametri Alfa-Beta validi, board ben inizializzata, stato corrente corrispondente al giocatore MAX (P2).
EFFECT: calcola il punteggio MAX dello stato corrente nell'albero minimax
RETURNS: struct mmretval per lo stato corrente. */
	int i, j;
	int bestrow = -1;
	int bestcol = -1;
	char found = 0;
	uint8_t *newboard;
	struct hashScore *hs = NULL;
	struct mmretval retval = DEF_MAX_MMRETVAL;
	struct mmretval mmtmp;
	
	/* Controlla se siamo a profondità massima. */
	if (ply >= maxplies) {
		retval.score = eval();
		retval.isExact = 0;
		return retval;
	}
	
	/* Cerca lo stato corrente nella tabella hash. Si noti che questo è fatto dopo il controllo di profondità, dal momento che gli stati a profondità massima sono sicuramente nuovi. */
	newboard = copyBoard();
	HASH_FIND(hh, scoreTable, newboard, len, hs);
	if (hs) {
		/* Hit. */
		if (hs->turn == -1) {
			/* L'entry è esatta e si può usare. */
			free(newboard);
			retval.score = hs->score;
			return retval;
		} else {
			/* L'entry non è esatta. */
			if (gameturn - hs->turn < AGE_CUTOFF) {
				 /* L'entry è abbastanza recente da poter essere usata. */
				free(newboard);
				retval.score = hs->score;
				retval.isExact = 0;
				return retval;
			} else {
				/* L'entry è troppo vecchia per poter essere usata: la rimuoviamo dalla tabella hash e procediamo come per un miss. */
				HASH_DEL(scoreTable, hs);
				free(hs);
			}
		}
	}

	/* Cerca successori. */
	for (j = 0; j < cols; j++) {
		if ((i = getNextBlank(j)) != -1) {
			 /* E' una mossa valida. */
			found = 1;
			setTok(board,TOKP2,i,j);
			if (isWinMove(i, j)) {
				/* La mossa porta immediatamente a vittoria. */
				bestrow = i;
				bestcol = j;
				retval.score = INT_MAX;
				setTok(board,BLANK,i,j);
				break;
			} else {
				/* E' necessaria una ricerca minimax. */
				mmtmp = minMove(a, b, ply + 1);
				retval.isExact &= mmtmp.isExact; /* Basta che uno di questi sia inesatto perché il valore corrente non lo sia a sua volta. */
				if (mmtmp.score > retval.score) {
					bestrow = i;
					bestcol = j;
					retval.score = mmtmp.score;
					/* Potatura Alfa-Beta. */
					if (mmtmp.score > a) {
						a = mmtmp.score;
						if (b <= a) {
							setTok(board,BLANK,i,j);
							break;
						}
					}
				}
				setTok(board,BLANK,i,j);
			}
		}
	}

	if (found) {
		/* Abbiamo trovato una mossa ottima valida e la dobbiamo riflettere su newboard. */
		if (bestrow == -1 && bestcol == -1) {
			/* Non abbiamo mai avuto un miglioramento, tutte le mosse sono egualmente orribili, prendiamo l'ultima valida. */
			bestrow = i;
			bestcol = j;
		}
		setTok(newboard,TOKP2,bestrow,bestcol);
	} else {
		/* Il tabellone è pieno, siamo in pareggio e newboard non richiede aggiornamenti. */
		retval.score = TIEVAL;
	}

	/* Aggiungiamo una entry alla tabella hash. */
	if ((hs = malloc(sizeof(struct hashScore))) == NULL) {
		perror("max hs malloc");
		exit(EXIT_FAILURE);
	}
	hs->key = newboard;
	hs->score = retval.score;
	(retval.isExact) ? (hs->turn = -1) : (hs->turn = gameturn);
	HASH_ADD_KEYPTR(hh, scoreTable, hs->key, len, hs);

	return retval;
}


struct mmretval minMove(int a, int b, int ply) {
/* REQUIRES: a, b parametri Alfa-Beta validi, board ben inizializzata, stato corrente corrispondente al giocatore MIN (P1).
EFFECT: calcola il punteggio MIN dello stato corrente nell'albero minimax
RETURNS: struct mmretval per lo stato corrente. */
	int i, j;
	int bestrow = -1;
	int bestcol = -1;
	char found = 0;
	uint8_t *newboard;
	struct hashScore *hs = NULL;
	struct mmretval retval = DEF_MIN_MMRETVAL;
	struct mmretval mmtmp;
			
	/* Controlla se siamo a profondità massima. */
	if (ply >= maxplies) {
		retval.score = eval();
		retval.isExact = 0;
		return retval;
	}

	/* Cerca lo stato corrente nella tabella hash. Si noti che questo è fatto dopo il controllo di profondità, dal momento che gli stati a profondità massima sono sicuramente nuovi. */
	newboard = copyBoard();
	HASH_FIND(hh, scoreTable, newboard, len, hs);
	if (hs) {
		/* Hit. */
		if (hs->turn == -1) {
			/* L'entry è esatta e si può usare. */
			free(newboard);
			retval.score = hs->score;
			return retval;
		} else {
			/* L'entry non è esatta. */
			if (gameturn - hs->turn < AGE_CUTOFF) {
				/* L'entry è abbastanza recente da poter essere usata. */
				free(newboard);
				retval.score = hs->score;
				retval.isExact = 0;
				return retval;
			} else {
				/* L'entry è troppo vecchia per poter essere usata: la rimuoviamo dalla tabella hash e procediamo come per un miss. */
				HASH_DEL(scoreTable, hs);
				free(hs);
			}
		}
	}

	/* Cerca successori. */
	for (j = 0; j < cols; j++) {
		if ((i = getNextBlank(j)) != -1) {
			/* La mossa è valida. */
			found = 1;
			setTok(board,TOKP1,i,j);
			if (isWinMove(i, j)) {
				/* La mossa porta immediatamente a vittoria. */
				bestrow = i;
				bestcol = j;
				retval.score = INT_MIN;
				setTok(board,BLANK,i,j);
				break;
			} else {
				/* E' necessaria una ricerca minimax. */
				mmtmp = maxMove(a, b, ply + 1);
				retval.isExact &= mmtmp.isExact; /* Basta che uno di questi sia inesatto perché il valore corrente lo sia a sua volta.s */
				if (mmtmp.score < retval.score) {
					bestrow = i;
					bestcol = j;
					retval.score = mmtmp.score;
					/* Potatura Alfa-beta. */
					if (mmtmp.score < b) {
						b = mmtmp.score;
						if (b <= a) {
							setTok(board,BLANK,i,j);
							break;
						}
					}
				}
				setTok(board,BLANK,i,j);
			}
		}
	}

	if (found) {
		/* Abbiamo trovato una mossa ottima valida e la dobbiamo riflettere su newboard. */
		if (bestrow == -1 && bestcol == -1) {
			/* Non abbiamo mai avuto un miglioramento, tutte le mosse sono egualmente orribili, prendiamo l'ultima valida. */
			bestrow = i;
			bestcol = j;
		}
		setTok(newboard,TOKP1,bestrow,bestcol);
	} else {
		/* Il tabellone è pieno, siamo in pareggio e newboard non richiede aggiornamenti. */
		retval.score = TIEVAL;
	}

	/* Aggiungiamo una entry alla tabella hash. */
	if ((hs = malloc(sizeof(struct hashScore))) == NULL) {
		perror("min hs malloc");
		exit(EXIT_FAILURE);
	}
	hs->key = newboard;
	hs->score = retval.score;
	(retval.isExact) ? (hs->turn = -1) : (hs->turn = gameturn);
	HASH_ADD_KEYPTR(hh, scoreTable, hs->key, len, hs);

	return retval;
}



int main(int argc, char* argv[]) {
	int i, j, opt, temp, p1move, p2move;

	/* Si leggono con una getopt standard gli argomenti passati da riga di comando. */
	while ((opt = getopt(argc, argv, "r:c:p:")) != -1) {
		switch (opt) {
		case 'r':
			errno = 0;
			temp = strtol(optarg, NULL, 10);
			if (errno != 0) {
				perror("Rows strtol");
				exit(EXIT_FAILURE);
			} else {
				if (temp == 0) {
					fprintf(stderr, "Couldn't read row number, using default value.\n");
				} else {
					rows = temp;
				}
			}
			break;
		case 'c':
			errno = 0;
			temp = strtol(optarg, NULL, 10);
			if (errno != 0) {
				perror("Cols strtol");
				exit(EXIT_FAILURE);
			} else {
				if (temp == 0) {
					fprintf(stderr, "Couldn't read column number, using default value.\n");
				} else {
					cols = temp;
				}
			}
			break;
		case 'p':
			errno = 0;
			temp = strtol(optarg, NULL, 10);
			if (errno != 0) {
				perror("Plies strtol");
				exit(EXIT_FAILURE);
			} else {
				if (temp == 0) {
					fprintf(stderr, "Couldn't read max minmax depth before heuristic eval, using default value.\n");
				} else {
					maxplies = temp;
				}
			}
			break;
		default:
			fprintf(stderr, "Usage: %s [-r rows] [-c cols] [-p maxplies]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	
	/* Calcoliamo la lunghezza fisica del vettore board. In un byte stanno quattro token. Si noti che ci serve il DIV+1. */
	len = rows*cols/4 +1;
	
	/* Si alloca il vettore board. Si noti che sizeof(uint8_t) è 1. */
	if ((board = calloc(len, 1)) == NULL) {
		perror("board calloc");
		exit(EXIT_FAILURE);
	}
	
	
	printf("GAME BEGINS, PLAYER'S TURN.\n\n");
	while (gameturn < rows*cols) {
		printBoard();
		if (gameturn % 2 == 0) {
			/* Turno del giocatore umano (P1), si legge la mossa da stdin. */
			temp = 0;
			do {
				if (temp == -1) {
					printf("Column %d is full!\n", p1move+1);
				}
				printf("Enter your next move: ");
				scanf("%d", &p1move);
				p1move--;
			} while (p1move < 0 || p1move >= cols || (temp = getNextBlank(p1move)) == -1) ;
			
			setTok(board,TOKP1,temp,p1move);
			if (isWinMove(temp, p1move)) {
				printBoard();
				printf("YOU WON! Game over.\n");
				exit(EXIT_SUCCESS);
			}
		} else {
			/* Turno della CPU (P2). */
			p2move = getNextP2Move();
			temp = getNextBlank(p2move);
			setTok(board,TOKP2,temp,p2move);
			if (isWinMove(temp, p2move)) {
				printBoard();
				printf("YOU LOST! Game over.\n");
				exit(EXIT_SUCCESS);
			}
		}
		gameturn++;
	}

	printBoard();
	printf("TIE! Game over.\n");
	exit(EXIT_SUCCESS);
}

